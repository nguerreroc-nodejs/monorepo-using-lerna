export class Math {

    public add(a: number, b: number): number {
        return a + b;
    }

    public div(a: number, b: number): number {
        if (b == 0)
            throw new Error("Div Zero Exception");            

        return a/b;
    }
}
